{
    "doku_inc": "/var/repos/dokuwiki/",
    "logger": {
        "api": {
            "rotate" : 3,
            "log_dir": "tmp/logs/",
            "path"   : "API.log",
            "level"  : "INFO",
            "stderr" : true,
            "buffer" : true
 
        }
    },
    "pdo": {
        "default": {
            "host"   : "192.168.254.198",
            "port"   : "3306",
            "name"   : "dbname",
            "charset": "utf8mb4",
            "user"   : "username",
            "pass"   : "password"
        }
    },
    "mailer": {
        "default": {
            "host"   : "smtp.domain.fqdn",
            "port"   : "587",
            "user"   : "username",
            "pass"   : "passwrod"
        }
    },
    "routing": {
        "apps:samples": "WebAPI\\Controller\\SampleController"
    }
}