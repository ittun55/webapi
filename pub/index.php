<?php
require_once __DIR__ . '/../vendor/autoload.php';
 
use WebAPI\Config;
use WebAPI\DIService;

try {
    $container = DIService::getContainer();
    $config    = $container['config'];
    $processLogger = $container['logger.processLog'];
    
    $reqid = uniqid('API-');
    $request = parseRequest();
 
    $resource = $request['resource'];
    $userinfo = $container['userinfo'];

    if (!$container['acl_callable']($resource, $userinfo['user'], $userinfo['grps'])) {
        error(900, ['_summary' => 'Forbidden'], $processLogger->flush());
    }

    if (!$config->hasValue('routing.'.$resource)) {
        $err = [ '_summaries' => ['Not Found'] ];
        error(901, $err, $processLogger->flush());
    }

    if (empty($request['action'])) {
        $err = [ '_summaries' => ['Not Found'] ];
        error(902, $err, $processLogger->flush());
    }
 
    $controller = $config->getValue('routing.'.$resource);
    $action = $request['action'];

    if (!method_exists($controller, $action)) {
        $err = [ '_summaries' => ['Not Found'] ];
        error(903, $err, $processLogger->flush());
    }

    $controller::setPDO($container['pdo.default']);
    $controller::setLogger($container['logger.api']);
    $controller::setMailer($container['mailer.default']);

    $action = $request['action'];
    $params = isset($request['params']) ? $request['params'] : null;

    // コントローラメソッドのレスポンスは成功時は [true, $data] 失敗時は [false, $errors] とし
    // コントローラからレスポンスを送出済みで、ここでの送出が不要な場合は [null, null] とする.
    list($result, $value) = $controller::$action($params, $userinfo);

    $debug = ($config->hasValue('debug') && $config->getValue('debug') === true);

    if ($result === null) {
    } elseif ($result === true && $debug === true) {
        send($value, $processLogger->flush());
    } elseif ($result === true && $debug !== true) {
        send($value);
    } else {
        error(900, $value, $processLogger->flush());
    }

} catch (Throwable $e) {
 
    $logger = $container['logger.api'];
    $logger->error($reqid.': '.$e->getMessage());
    $logger->error($e->getTraceAsString());
 
    $err = [];
    $err['_summaries'] = [$e->getMessage()];
    $err['_trace']     =  $e->getTrace();
    error(500, $err, $processLogger->flush());
}
 
function parseRequest(): ?array {
    // リクエストボディをデコード
    $content_type = explode(';', trim(strtolower($_SERVER['CONTENT_TYPE'])));
    $media_type = $content_type[0];
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $media_type == 'application/json') {
        // application/json で送信されてきた場合の処理
        return json_decode(file_get_contents('php://input'), true);
    } elseif ($_SERVER['REQUEST_METHOD'] == 'PUT' && $media_type == 'application/json') {
        // application/json で送信されてきた場合の処理
        return json_decode(file_get_contents('php://input'), true);
    } else {
        return null;
    }
}
 
function error(int $code, ?array $errors, ?array $logs=[], bool $setStatus=false): void {
    if ($code === 200) {
        $code = 500;
        $msg  = 'エラー時のコードは 200 以外を指定してください.';
        if (!in_array('_summaries', $errors)) {
            $errors['_summaries'] = [];
        }
        $errors['_summaries'][] = $msg;
    }
    if ($setStatus) http_response_code($code);
    header('content-type: application/json; charset=utf-8');
    $body = [ '_code' => $code, '_errors' => $errors, '_logs' => $logs ];
    echo json_encode($body, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    exit();
}
 
/**
 * 
 * @param null|array $data この連想配列内には item または items というプロパティが存在していることを想定 
 * @param null|array $logs サーバーサイドでの処理ログ
 * @return void 
 */
function send(?array $data, $debug=false, ?array $logs=null): void {
    header('content-type: application/json; charset=utf-8');
    $data['_code'] = 200;
    if ($debug) $data['_logs'] = $logs;
    echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    exit();
}