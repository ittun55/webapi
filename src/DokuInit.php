<?php
namespace WebAPI;

use WebAPI\DIService;

global $USERINFO; // DokuWiki のセッションに紐づくユーザー情報は DOKU_INC/inc/init.php 実行でここに格納される
global $ACL_CALLABLE;

// DokuWiki による Cookie セッションの解読
$container = DIService::getContainer();
if (!defined('DOKU_INC')) define('DOKU_INC', $container['config']->getValue('doku_inc'));
if (!defined('NL')) define('NL', "\n");
define('DOKU_DISABLE_GZIP_OUTPUT', 1);
require_once(DOKU_INC.'inc/init.php');
$USERINFO['user'] = $_SERVER['REMOTE_USER'];
session_write_close();  //close session

/**
 * DOKU_INC/inc/auth.php に定義がある
 * 0 => 'AUTH_NONE',
 * 1 => 'AUTH_READ',
 * 2 => 'AUTH_EDIT',
 * 4 => 'AUTH_CREATE',
 * 8 => 'AUTH_UPLOAD',
 * 16 => 'AUTH_DELETE',
 * 255 => 'AUTH_ADMIN'
 * @param $resource
 * @param $user
 * @param $groups
 * @return bool
 * @throws Exception
 */
$ACL_CALLABLE = function($resource, $user, $groups): bool {
    $acl_func = 'auth_aclcheck';
    return ($acl_func($resource, $user, $groups) >= 1);
};