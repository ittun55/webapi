<?php declare(strict_types=1);
namespace WebAPI;
 
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
 
class ProcessLogger extends AbstractProcessingHandler
{
    protected $buffer = [];
 
    public function __construct($level = Logger::DEBUG, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
    }
 
    public function isHandling(array $record): bool
    {
        return true;
    }

    public function write(array $record): void
    {
        $this->buffer[] = $record;
    }
 
    public function flush(): array
    {
        $formatted = array_map(function($rec) {
            return (string) $rec['formatted'];
        }, $this->buffer);
        $this->buffer = [];
        return $formatted;
    }
}