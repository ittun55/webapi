<?php
namespace WebAPI;

use Exception;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use PDO;
use Pimple\Container as Pimple;
use WebAPI\Model\Model;

class DIService
{
    private static $container;

    public static function initialize()
    {
        self::$container = new Pimple();
        self::initializeConfig();
        self::initializePdo();
        self::initializeLogger();
        self::initializeMailer();
        self::initializeAuthentication();
    }

    public static function getContainer(): Pimple
    {
        if (is_null(self::$container))
            self::initialize();
        return self::$container;
    }

    public static function initializeConfig()
    {
        self::$container['config'] = function() {
            return Config::load();
        };
    }

    public static function initializeLogger()
    {
        // ProcessLogger は取得したログをクライアントに返すためのクラス
        $processLogger = new ProcessLogger();
        $processLogger->setFormatter(new LineFormatter(null, null, true, true));

        self::$container['logger.processLog'] = $processLogger;

        foreach(self::$container['config']->getValue('logger') as $name => $setting) {
            $logger = new Logger($name);
            $path   = self::getActualPath($setting['log_dir']) . $setting['path'];
            $level  = $setting['level'];
            $rotate = $setting['rotate'];
            if (!file_exists(dirname($path)) || !is_writable(dirname($path)))
                throw new Exception("log dir ${path} is not exists or not writable.");
            $fileHandler = new RotatingFileHandler($path, $rotate, $level);
            $formatter = new LineFormatter();
            $formatter->includeStacktraces(true);
            $logger->pushHandler($fileHandler->setFormatter($formatter));
            if ($setting['stderr']) {
                $stderr = $setting['stderr'];
                $streamHandler = new StreamHandler('php://stderr', $setting['level']);
                $logger->pushHandler($streamHandler);
            }
            $logger->pushHandler($processLogger);
            self::$container['logger.'.$name] = $logger;
        }
    }

    /**
     * 相対パスからフルパスへの変換
     *
     * @param string $path
     * @return string
     */
    public static function getActualPath(string $path): string
    {
        return (substr($path, 0, 1) === '/') ? $path  : __DIR__ . '/../' . $path;
    }

    public static function initializePdo()
    {
        foreach(self::$container['config']->getValue('pdo') as $conn => $setting) {
            $host     = $setting['host'];
            $port     = $setting['port'];
            $dbname   = $setting['name'];
            $charset  = $setting['charset'];
            $username = $setting['user'];
            $password = $setting['pass'];
            $dsn = "mysql:host=${host};port=${port};dbname=${dbname};charset=${charset}";
            $options  = [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_STRINGIFY_FETCHES => false
            ];
            self::$container['pdo.'.$conn] = new PDO($dsn, $username, $password, $options);
        }
    }

    public static function initializeMailer()
    {
        foreach(self::$container['config']->getValue('mailer') as $name => $setting) {
            self::$container['mailer.'.$name] = function() use ($setting) {
                $user = $setting['user'];
                $pass = $setting['pass'];
                $host = $setting['host'];
                $port = $setting['port'];
                $dsn = "smtp://${user}:${pass}@${host}:${port}";
                return new SendMail($dsn);
            };
        }
    }

    public static function initializeModel(): void
    {
        Model::setLogger(self::$container['logger.default']);
    }

    public static function initializeAuthentication(): void
    {
        global $USERINFO, $ACL_CALLABLE;
        require_once('DokuInit.php');
        self::$container['userinfo'] = $USERINFO;
        self::$container['acl_callable'] = function() use ($ACL_CALLABLE) {
            return $ACL_CALLABLE;
        };
    }
}
