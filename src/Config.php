<?php
namespace WebAPI;
 
class Config
{
    protected $_data;
 
    public function __construct(?array $kv=null)
    {
        if (is_null($kv)) return;
        foreach ($kv as $k => $v) {
            $this->_data[$k] = $v;
        }
    }

    /**
     * 設定ファイルの PATH は $PRJ_ROOT/cnf/config.json 固定
     * @return void
     */
    public static function load($path=null): Config
    {
        $config_path = ($path) ? self::getActualPath($path) : __DIR__.'/../cnf/config.json';

        if (!file_exists($config_path)) {
            throw new \InvalidArgumentException($config_path . ' not found. Please specify existing file.');
        }

        if (!$file = file_get_contents($config_path)) {
            throw new \InvalidArgumentException($config_path . ' could not read. Please confirm access rights.');
        };

        $_data = json_decode($file, true);
        return new static($_data);
    }

    /**
     * 相対パスからフルパスへの変換
     *
     * @param string $path
     * @return string
     */
    public static function getActualPath(string $path): string
    {
        return (substr($path, 0, 1) === '/') ? $path  : __DIR__ . '/../' . $path;
    }

    public function hasValue(?string $path=null, string $delimiter='.')
    {
        if (is_null($path)) return true;
        try {
            $this->getValue($path, $delimiter);
            return true;
        } catch (\OutOfRangeException $e) {
            return false;
        }
    }
 
    public function getValue(?string $path=null, string $delimiter='.')
    {
        if (is_null($path)) return self::$_data;
 
        $nodes = explode($delimiter, $path);
        $value = $this->_data;
 
        foreach ($nodes as $node) {
            if ($node === '') continue;
            if (preg_match('/(?P<prop>\w+)\[(?P<idx>\d*)\]/', $node, $matches)) {
                $prop = $matches['prop'];
                $idx  = (int)$matches['idx'];
                if (!array_key_exists($prop, $value) || !array_key_exists($idx, $value[$prop])) {
                    throw new \OutOfRangeException('The config has no value for '.$path);
                }
                $value = $value[$prop][$idx];
            } else {
                if (!array_key_exists($node, $value)) {
                    throw new \OutOfRangeException('The config has no value for '.$path);
                }
                $value = $value[$node];
            }
        }
        return $value;
    }
}