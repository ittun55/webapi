<?php
namespace WebAPI\Controller;

use PDO;
use Psr\Log\LoggerInterface;
use WebAPI\Controller\ControllerInterface;
use WebAPI\SendMail;

class Controller implements ControllerInterface
{
    protected static $pdo = null;
    protected static $logger = null;
    protected static $mailer = null;

    public static function setPDO(PDO $pdo): void
    {
        static::$pdo = $pdo;
    }

    public static function getPDO(): PDO
    {
        return static::$pdo;
    }

    public static function setLogger(LoggerInterface $logger): void
    {
        static::$logger = $logger;
    }

    public static function getLogger(): LoggerInterface
    {
        return static::$logger;
    }

    public static function setMailer(SendMail $mailer): void
    {
        static::$mailer = $mailer;
    }

    public static function getMailer(): SendMail
    {
        return static::$mailer;
    }

    public static function hasPaginateParams($params)
    {
        return (isset($params['page']) && $params['limit']);
    }
}