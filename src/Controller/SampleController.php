<?php
namespace WebAPI\Controller;

class SampleController extends Controller
{
    public static function index()
    {
        return [true, ['item' => 'sample']];
    }
}