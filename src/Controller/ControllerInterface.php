<?php
namespace WebAPI\Controller;

use PDO;
use Psr\Log\LoggerInterface;
use WebAPI\SendMail;

interface ControllerInterface
{
    public static function setPDO(PDO $pdo): void;
    public static function getPDO(): PDO;

    public static function setLogger(LoggerInterface $logger): void;
    public static function getLogger(): LoggerInterface;

    public static function setMailer(SendMail $mailer): void;
    public static function getMailer(): SendMail;
}