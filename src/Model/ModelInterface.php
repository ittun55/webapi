<?php
namespace WebAPI\Model;

use Psr\Log\LoggerInterface;

interface ModelInterface
{
    public static function setLogger(LoggerInterface $logger): void;
    public static function getLogger(): LoggerInterface;
    public static function loadSchema($schema): void;
}