<?php
require_once __DIR__ . '/../vendor/autoload.php';

use WebAPI\Config;
use iamcal\SQLParser;

class DumpSchema
{
    public static function main(array $argv)
    {
        $msg = "arg1: table name to dump schema." . PHP_EOL;

        Config::load();

        // argv[1] テーブル名の確認
        if (empty($argv[1])) {
            die($msg);
        }
        if (empty($argv[1])) die("table name must be specified.");

        // PDO の生成
        $pdo = self::createPdo(Config::getValue('pdo.default'));

        // テーブル定義の出力
        $def = self::getTableDefs($pdo, $argv[1]);
        $def_json = json_encode($def, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        if (empty($argv[2])) {
            echo $def_json . PHP_EOL;
        } else {
            echo "output to ../src/Model/${argv[1]}.json" . PHP_EOL;
            file_put_contents(__DIR__."/../src/Model/${argv[1]}.json", $def_json);
        }
    }

    private static function createPdo(array $setting): PDO
    {
        $host     = $setting['host'];
        $port     = $setting['port'];
        $dbname   = $setting['name'];
        $charset  = $setting['charset'];
        $username = $setting['user'];
        $password = $setting['pass'];
        $dsn = "mysql:host=${host};port=${port};dbname=${dbname};charset=${charset}";
        $options  = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_STRINGIFY_FETCHES => false
        ];
        return new PDO($dsn, $username, $password, $options);
    }
    
    private static function getTableDefs(\PDO $pdo, $table_name): array
    {
        $stmt = $pdo->prepare("SHOW CREATE TABLE `${table_name}`");
        $stmt->execute();
        $ct = $stmt->fetch();
        $parser = new SQLParser();
        $table = $ct['Table'];
        $parser->parse($ct['Create Table']);
        return $parser->tables[$table];
    }
}

DumpSchema::main($argv);